'use strict';
const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const PORT = 8180;
const path = require('path');
let isAuth = false;
app.use(function checkAuth(req, res, next) {
    isAuth = req.isAuth();
    return next();
});
app.use(express.static( path.join(__dirname, 'public')));
app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());


app.listen(PORT, () => {
    console.log('Server started at ' + PORT);
});