import {action, observable} from 'mobx';
import '@babel/polyfill';


//FIXME filename, not path
class mainStore {
    @observable pathToAudio = null;
    @observable audioContext = null;
    @observable analyser = null;
    @observable bufferLength = 1024;
    @observable dataBuffer = new Uint8Array(this.bufferLength);
    @observable data = [0];
    @observable dataPlot = {
        x: [0], y: [0], type: 'heatmap',
        colorscale: 'Viridis'
    };

    @action.bound
    async createAudioEnv(audioContext) {
        if (audioContext !== null) {
            console.log('ac:', audioContext);
            let analyser = audioContext.createAnalyser();
            analyser.fftSize = 2048;
            let bufferLength = analyser.frequencyBinCount;
            let processor = audioContext.createScriptProcessor(bufferLength);
            processor.onaudioprocess = function () {
                console.log('processing')
            };
            console.log('processor', processor);
            analyser.connect(audioContext.destination);

            this.audioContext = audioContext;
            this.analyser = analyser;
            this.bufferLength = bufferLength;
            this.dataBuffer = new Uint8Array(bufferLength);
            return true;
        }
        else {
            console.error('ENV creation failed');
            return false;
        }
    };

    @action.bound
    uploadFile(file) {
        const url = "http://localhost:4200/upload";
        const formData = new FormData();
        formData.append('file', file);
        fetch(url, {
            method: 'POST',
            body: formData
        }).then(res => res.json()).then(data => {
            console.log("Diiz:", data);
            this.pathToAudio = data.filename;
        }).catch(error => console.error('Error:', error));
    };

    @action.bound
    processFile() {
        this.analyser.getByteFrequencyData(this.dataBuffer);
        this.data = this.data.concat([...this.dataBuffer]);
        console.log(this.data);
        let newDelta = [...this.dataBuffer.map((el, i) => this.data.length + i)];
        let newX = [...this.dataPlot.x, ...newDelta];
        console.log('newd', newDelta);
        this.dataPlot = {
            ...this.dataPlot,
            y: this.data,
            x: newX,
        };
    };
}

export default new mainStore();