import React from 'react';
import {inject, observer} from 'mobx-react';

@inject('mainStore')
@observer
class Main extends React.Component {
    constructor(props) {
        super(props);
    };

    componentDidMount() {
    }

    componentWillUnmount() {
    }

    render() {
        return (<div>
            <header>Header</header>
        </div>);
    }
}

export default Main;