import React from 'react';
import styled from 'styled-components';

const StyledTable = styled.table`
    max-width: 700px;
    min-width: 500px;
    text-align: center;
    font-size: 30px;
    border-collapse: collapse;
    border-style: hidden;
td, th {
    border: 1px solid lightgray;
};`;


class Table extends React.Component {
    constructor(props) {
        super(props);
    };


    render() {
        const {rates} = this.props;
        return (
            <Wrapper>
                <thead>
                <tr>
                    <th>currency</th>
                    <th>buy</th>
                    <th>sell</th>
                </tr>
                </thead>
                <tbody>
                {rates.map(rate => <RateRow key={rate.ccy + rate.base_ccy} rate={rate}/>)}
                </tbody>
            </Wrapper>);

    };
}


const RateRow = ({rate}) => <tr>
        <td>{rate.ccy + " to " + rate.base_ccy}</td>
        <td>{rate.buy}</td>
        <td>{rate.sale}</td>
    </tr>
;

//Had to use wrapper to define Styled Components outside of the render
const Wrapper = ({children}) => {
    return <StyledTable>{children}</StyledTable>
};

export default Table;
