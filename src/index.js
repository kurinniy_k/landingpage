import React from 'react';
import ReactDOM from 'react-dom';
import {Provider} from 'mobx-react';
import mainStore from './stores/mainStore';
import Main from './components/Main';

ReactDOM.render(
    <Provider mainStore={mainStore}>
        <Main/>
    </Provider>,
    document.getElementById('app')
);